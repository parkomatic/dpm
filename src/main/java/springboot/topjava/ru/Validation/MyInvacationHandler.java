package springboot.topjava.ru.Validation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyInvacationHandler implements InvocationHandler {
    private Object obj;

    public MyInvacationHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        try {
            if (method.getName().indexOf("returnNumber")>-1)
            {
                System.out.println("Проверка номера машины");
            }
            result=method.invoke(obj,args);

        }
        catch (InvocationTargetException e){
            throw e;}


        return result;
    }
}
