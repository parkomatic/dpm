package springboot.topjava.ru.ModemsParts;

import org.springframework.web.bind.support.SessionStatus;

import java.security.SecureRandom;

public class SessionPoolModel {
    public String id;
    public String createAt;
    private SessionStatus status;
    private String paredAt;
    private Object renewals;
    private String faildeAt;
    private SecureRandom vehicle;
    private int parking;
    private String device;
    private int initialID;
    public Object modembox;

    public SessionPoolModel() {
    }

    public SessionPoolModel(String id, String createAt, SessionStatus status, String paredAt, Object renewals, String faildeAt, SecureRandom vehicle, int parking, String device, int initialID, Object modembox) {
        this.id = id;
        this.createAt = createAt;
        this.status = status;
        this.paredAt = paredAt;
        this.renewals = renewals;
        this.faildeAt = faildeAt;
        this.vehicle = vehicle;
        this.parking = parking;
        this.device = device;
        this.initialID = initialID;
        this.modembox = modembox;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public SessionStatus getStatus() {
        return status;
    }

    public void setStatus(SessionStatus status) {
        this.status = status;
    }

    public String getParedAt() {
        return paredAt;
    }

    public void setParedAt(String paredAt) {
        this.paredAt = paredAt;
    }

    public Object getRenewals() {
        return renewals;
    }

    public void setRenewals(Object renewals) {
        this.renewals = renewals;
    }

    public String getFaildeAt() {
        return faildeAt;
    }

    public void setFaildeAt(String faildeAt) {
        this.faildeAt = faildeAt;
    }

    public SecureRandom getVehicle() {
        return vehicle;
    }

    public void setVehicle(SecureRandom vehicle) {
        this.vehicle = vehicle;
    }

    public int getParking() {
        return parking;
    }

    public void setParking(int parking) {
        this.parking = parking;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public int getInitialID() {
        return initialID;
    }

    public void setInitialID(int initialID) {
        this.initialID = initialID;
    }

    public Object getModembox() {
        return modembox;
    }

    public void setModembox(Object modembox) {
        this.modembox = modembox;
    }
}
