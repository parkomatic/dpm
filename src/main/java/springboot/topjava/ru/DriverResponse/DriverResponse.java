package springboot.topjava.ru.DriverResponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import springboot.topjava.ru.DataRealizations.Data;
import springboot.topjava.ru.ForProxyLogging.DriverResponseInterf;


@JsonPropertyOrder({"ReturnCode","Data","debug","DebugMsg"})
public class DriverResponse implements DriverResponseInterf {

    @JsonProperty("ReturnCode")
    private int returnCode;
    @JsonProperty("Data")
    private Data data;
    @JsonProperty("debug")
    private String debug;
    @JsonProperty("DebugMsg")
    private String debugMsg;

    public DriverResponse() {
    }

    public DriverResponse(int returnCode, Data data, String debug, String debugMsg) {
        this.returnCode = returnCode;
        this.data = data;
        this.debug = debug;
        this.debugMsg = debugMsg;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getDebug() {
        return debug;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }

    public String getDebugMsg() {
        return debugMsg;
    }

    public void setDebugMsg(String debugMsg) {
        this.debugMsg = debugMsg;
    }
}