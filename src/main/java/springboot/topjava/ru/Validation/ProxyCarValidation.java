package springboot.topjava.ru.Validation;

import java.lang.reflect.Proxy;

public class ProxyCarValidation {
    public static Object newInstance (Object ob)
    {
        return Proxy.newProxyInstance(ob.getClass().getClassLoader(),new Class<?>[] {CarValidation.class}, new MyInvacationHandler(ob));
    }
}
