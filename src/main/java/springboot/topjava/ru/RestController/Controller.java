package springboot.topjava.ru.RestController;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import springboot.topjava.ru.DataRealizations.DataAuth;
import springboot.topjava.ru.DataRealizations.DataPark;
import springboot.topjava.ru.DataRealizations.DataReset;
import springboot.topjava.ru.DriverResponse.DriverResponse;
import springboot.topjava.ru.ForProxyLogging.CheckVizozInvocationHandler;
import springboot.topjava.ru.ForProxyLogging.DriverResponseInterf;
import springboot.topjava.ru.ModemsParts.RawStatePool;
import springboot.topjava.ru.VladApi.ForAuth.LogikaVladaAuthInterface;
import springboot.topjava.ru.api_auth.RequestAuth;
import springboot.topjava.ru.api_auth.RequestParking;
import springboot.topjava.ru.api_auth.RequestReset;

import java.lang.reflect.Proxy;

import static springboot.topjava.ru.BD.BDNumberModem.countaccesskey;

@RestController
@RequestMapping("/api")
public class Controller {
    private final Integer password = 123;
    private static Logger logger = LoggerFactory.getLogger(Controller.class);


    @Autowired
    @Qualifier("dev")
    LogikaVladaAuthInterface logikaVlada;

    @RequestMapping("/log")
    public String index() {
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message");

        return "Howdy! Check out the Logs to see the output...";
    }


    @PostMapping("/auth")
    public DriverResponse auth(@RequestBody RequestAuth request) {


        DriverResponse driverResponse = new DriverResponse();
        /*
        int k=request.getAccessKey;
        ResultSet rs = stmt.executeQuery( "SELECT * FROM NUMBERMODEM WHERE ID="+idnum+";" );
         */
        int countkey = countaccesskey(request.getAcceskey());
        if (countkey >0) {
            DataAuth data = new DataAuth();
            data.setSessionkey(logikaVlada.auth());

            DriverResponseInterf proxy=(DriverResponseInterf) Proxy.newProxyInstance(driverResponse.getClass().getClassLoader(),
                    new Class<?>[] { DriverResponseInterf.class }, new CheckVizozInvocationHandler(driverResponse));
            proxy.setData(data);

            //driverResponse.setData(data);
            driverResponse.setReturnCode(1);
            driverResponse.setDebug("Debug");
            driverResponse.setDebugMsg("DebugMsg");

        }

        return driverResponse;

    }

    @PostMapping("/parking")
    public DriverResponse parking(@RequestBody RequestParking request) {

        DriverResponse driverResponse = new DriverResponse();
        DataPark data = new DataPark();
        System.out.println(request.getAction());
        switch (request.getAction()){
            case "0":
            {
                System.out.println("0");
                break;
            }
            case "1":
            {
                System.out.println("1");
                break;
            }
            case "2":
            {
                data.setSessionId(logikaVlada.renew());
                break;
            }
            case "3":
            {
                data.setSessionId(logikaVlada.cancel());
                break;
            }
            case "4":
            {
                data.setSessionId(logikaVlada.parkovka(request.getCarNumber(), request.getStayID()));
                break;
            }

        }

       // if (request.getAction() != null)
        {
           // DataPark data = new DataPark();
           // data.setSessionId(logikaVlada.parkovka(request.getCarNumber(), request.getStayID()));

            data.setCode(1);
            data.setParkId("1234554321");

            driverResponse.setData(data);
            driverResponse.setDebug("Debug");
            driverResponse.setReturnCode(2);
            driverResponse.setDebugMsg("DebugMsg");
        }
        return driverResponse;
    }

    @PostMapping("/reset")

    public DriverResponse reset(@RequestBody RequestReset request) {
        DriverResponse driverResponse = new DriverResponse();
        if (request.getData() != null) {
            DataReset dataReset = new DataReset();
            dataReset.setPole("Bla Bla time to reset");
            //driverResponse.setData(dataReset);

            driverResponse.setDebug("Debug");
            driverResponse.setReturnCode(3);
            driverResponse.setDebugMsg("DebugMsg");

        }

        return driverResponse;

    }

    @GetMapping("raw-state")
    public RawStatePool rawstate() {
        RawStatePool rawStatePool=new RawStatePool();


        return rawStatePool;


    }









    }