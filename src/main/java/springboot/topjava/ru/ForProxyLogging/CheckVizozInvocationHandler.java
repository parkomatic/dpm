package springboot.topjava.ru.ForProxyLogging;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CheckVizozInvocationHandler implements InvocationHandler {
    public CheckVizozInvocationHandler(Object obj) {
        this.obj = obj;
    }

    //private static Logger LOGGER = LoggerFactory.getLogger(CheckVizozInvocationHandler.class);
    private Object obj

            ;


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;

        if (method.getName().indexOf("setData") > -1) {
            System.out.println("getData is executing");
        }
        result = method.invoke(obj, args);
        return result;
    }
}