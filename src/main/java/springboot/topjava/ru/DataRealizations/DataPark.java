package springboot.topjava.ru.DataRealizations;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"Code","SessionID","ParkID"})
public class DataPark implements Data{
    @JsonProperty("Code")
    private int code;
    @JsonProperty("SessionID")
    private String sessionId;
    @JsonProperty("ParkID")
    private String parkId;

    public DataPark() {
    }

    public DataPark(int code, String sessionId, String parkId) {
        this.code = code;
        this.sessionId = sessionId;
        this.parkId = parkId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }
}
