package springboot.topjava.ru.api_auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestParking {
   @JsonProperty("Action")
    private String action;
    @JsonProperty("CarNumber")
    private String carNumber;
    @JsonProperty("DeviceNumber")
    private String deviceNumber;
    @JsonProperty("StayID")
    private String stayID;
    @JsonProperty("Time")
    private String time;
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getStayID() {
        return stayID;
    }

    public void setStayID(String stayID) {
        this.stayID = stayID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public RequestParking(String action, String carNumber, String deviceNumber, String stayID, String time) {
        this.action = action;
        this.carNumber = carNumber;
        this.deviceNumber = deviceNumber;
        this.stayID = stayID;
        this.time = time;
    }
}
