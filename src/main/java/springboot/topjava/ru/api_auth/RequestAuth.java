package springboot.topjava.ru.api_auth;

public class RequestAuth {
   private String login;
   private String password;
   private String acceskey;

    public RequestAuth() {
    }

    public RequestAuth(String login, String password, String acceskey) {
        this.login = login;
        this.password = password;
        this.acceskey = acceskey;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAcceskey() {
        return String.valueOf(acceskey);
    }

    public void setAcceskey(String acceskey) {
        this.acceskey = acceskey;
    }
}
