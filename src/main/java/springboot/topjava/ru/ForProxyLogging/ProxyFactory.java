package springboot.topjava.ru.ForProxyLogging;

import java.lang.reflect.Proxy;

public class ProxyFactory {
    public static Object newInstance(Object ob) {
        return Proxy.newProxyInstance(ob.getClass().getClassLoader(),
                new Class<?>[] { DriverResponseInterf.class }, new CheckVizozInvocationHandler(ob));
    }
}