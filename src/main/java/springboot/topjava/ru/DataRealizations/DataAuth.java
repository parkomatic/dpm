package springboot.topjava.ru.DataRealizations;

public class DataAuth implements Data {
    private String Sessionkey;

    public DataAuth() {
    }

    public DataAuth(String sessionkey) {
        Sessionkey = sessionkey;
    }

    public String getSessionkey() {
        return Sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        Sessionkey = sessionkey;
    }
}
