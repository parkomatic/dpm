package springboot.topjava.ru.Validation;

public interface CarValidation {
    boolean checkNumberCar(String numberCar);

    boolean checkNumberPark(String numberPark);

    String returnNumber(String numberCar);

    boolean checkNumberPhone(String numberPhone);

    boolean checkPin(String pin);
}
