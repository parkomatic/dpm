package springboot.topjava.ru.ModemsParts;

import java.util.List;

public class ModemPoolModel {

    private String imei;
    private String session;
    private String device;
    private List<String>ports;
    private String vid=null;
    private String pid=null;
    private String  manufactorer=null;
    private String product=null;
    private String port=null;
    private String notifyPort=null;
    private String modem=null;
    private SignalModel signalModel=null;
    private int balnce=0;

    public ModemPoolModel() {
    }

    public ModemPoolModel(String imei, String session, String device, List<String> ports, String vid, String pid, String manufactorer, String product, String port, String notifyPort, String modem, SignalModel signalModel, int balnce) {
        this.imei = imei;
        this.session = session;
        this.device = device;
        this.ports = ports;
        this.vid = vid;
        this.pid = pid;
        this.manufactorer = manufactorer;
        this.product = product;
        this.port = port;
        this.notifyPort = notifyPort;
        this.modem = modem;
        this.signalModel = signalModel;
        this.balnce = balnce;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public List<String> getPorts() {
        return ports;
    }

    public void setPorts(List<String> ports) {
        this.ports = ports;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getManufactorer() {
        return manufactorer;
    }

    public void setManufactorer(String manufactorer) {
        this.manufactorer = manufactorer;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getNotifyPort() {
        return notifyPort;
    }

    public void setNotifyPort(String notifyPort) {
        this.notifyPort = notifyPort;
    }

    public String getModem() {
        return modem;
    }

    public void setModem(String modem) {
        this.modem = modem;
    }

    public SignalModel getSignalModel() {
        return signalModel;
    }

    public void setSignalModel(SignalModel signalModel) {
        this.signalModel = signalModel;
    }

    public int getBalnce() {
        return balnce;
    }

    public void setBalnce(int balnce) {
        this.balnce = balnce;
    }
}
