package springboot.topjava.ru.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RealCarValidation implements CarValidation{

    @Override
    public boolean checkNumberCar(String numberCar) {
        Pattern p = Pattern.compile("^[abekmhopctyxABEKMHOPCTYX]\\d{3}[abekmhopctyxABEKMHOPCTYX]{2}\\d{2,3}$");
        Matcher m = p.matcher(numberCar);
        return m.matches();
    }

    @Override
    public boolean checkNumberPark(String numberPark) {
        Pattern p = Pattern.compile("^\\d{4}$");
        Matcher m = p.matcher(numberPark);
        return m.matches();
    }

    @Override
    public String returnNumber(String numberCar) {
        String s = numberCar;
        numberCar = "";
        char[] charsnumbercar = s.toCharArray();
        for (char i:charsnumbercar)
        {
            switch (i)
            {
                case 'а':{i='A';break;}
                case 'в':{i='B';break;}
                case 'е':{i='E';break;}
                case 'к':{i='K';break;}
                case 'м':{i='M';break;}
                case 'н':{i='H';break;}
                case 'о':{i='O';break;}
                case 'р':{i='P';break;}
                case 'с':{i='C';break;}
                case 'т':{i='T';break;}
                case 'у':{i='Y';break;}
                case 'х':{i='X';break;}

                case 'А':{i='A';break;}
                case 'В':{i='B';break;}
                case 'Е':{i='E';break;}
                case 'К':{i='K';break;}
                case 'М':{i='M';break;}
                case 'Н':{i='H';break;}
                case 'О':{i='O';break;}
                case 'Р':{i='P';break;}
                case 'С':{i='C';break;}
                case 'Т':{i='T';break;}
                case 'У':{i='Y';break;}
                case 'Х':{i='X';break;}
            }
            numberCar=numberCar+String.valueOf(i);
        }
        return numberCar;
    }

    @Override
    public boolean checkNumberPhone(String numberPhone) {
        Pattern p = Pattern.compile("^\\d{10}(?<!0000000000)$");
        Matcher m = p.matcher(numberPhone);
        return m.matches();
    }

    @Override
    public boolean checkPin(String pin) {
        Pattern p = Pattern.compile("^\\d{4}$");
        Matcher m = p.matcher(pin);
        return m.matches();
    }
}
