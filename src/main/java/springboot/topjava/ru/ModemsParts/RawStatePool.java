package springboot.topjava.ru.ModemsParts;

import java.util.List;

public class RawStatePool {

    private List<ModemPoolModel>modems;
    private List<SessionPoolModel>sessions;
    private Object authSessions;
    private Object backUp;

    public RawStatePool() {
    }

    public RawStatePool(List<ModemPoolModel> modems, List<SessionPoolModel> sessions, Object authSessions, Object backUp) {
        this.modems = modems;
        this.sessions = sessions;
        this.authSessions = authSessions;
        this.backUp = backUp;
    }

    public List<ModemPoolModel> getModems() {
        return modems;
    }

    public void setModems(List<ModemPoolModel> modems) {
        this.modems = modems;
    }

    public List<SessionPoolModel> getSessions() {
        return sessions;
    }

    public void setSessions(List<SessionPoolModel> sessions) {
        this.sessions = sessions;
    }

    public Object getAuthSessions() {
        return authSessions;
    }

    public void setAuthSessions(Object authSessions) {
        this.authSessions = authSessions;
    }

    public Object getBackUp() {
        return backUp;
    }

    public void setBackUp(Object backUp) {
        this.backUp = backUp;
    }
}
