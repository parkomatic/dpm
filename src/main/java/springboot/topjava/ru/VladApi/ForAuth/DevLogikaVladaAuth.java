package springboot.topjava.ru.VladApi.ForAuth;

import org.springframework.stereotype.Component;
import ru.dankovtsev.MainSikulixStartParkomatic;
import springboot.topjava.ru.DataRealizations.Data;
import springboot.topjava.ru.Validation.CarValidation;
import springboot.topjava.ru.Validation.ProxyCarValidation;
import springboot.topjava.ru.Validation.RealCarValidation;

import static springboot.topjava.ru.BD.BDNumberModem.parkomativBDnumPhone;


@Component("dev")

public class DevLogikaVladaAuth implements LogikaVladaAuthInterface {
    CarValidation carValidation = (CarValidation) ProxyCarValidation.newInstance(new RealCarValidation());

    @Override
    public String auth() {
        String[] numInfo=parkomativBDnumPhone(1);
        String login = numInfo[0];
        String password = numInfo[1];
        System.out.println(login);
        System.out.println(password);
        String result="";
        boolean check = false;

         check = MainSikulixStartParkomatic.checkLOgInLK();
         if (!check)
         {
             /*System.out.println(carValidation.checkNumberPhone(login));
             System.out.println(carValidation.checkPin(password));
             System.out.println(password);*/
            if((carValidation.checkNumberPhone(login))&&(carValidation.checkPin(password)))
             {
                 check=MainSikulixStartParkomatic.enterInApplication(login,password);
             }
         }

         if (check)
         {
             result = "success";
         }
         else
         {
             result = "no success";
         }
        return result;
    }

    @Override
    public String parkovka(String numberCar, String numberOfParking) {
        String result="";
        boolean check = false;
        boolean ch=false;
        check = MainSikulixStartParkomatic.checkLOgInLK();
        if (check)
        {
            System.out.println(carValidation.checkNumberCar(numberCar));
            System.out.println(carValidation.checkNumberPark(numberOfParking));
            if((carValidation.checkNumberCar(numberCar))&&(carValidation.checkNumberPark(numberOfParking))) {
                numberCar= carValidation.returnNumber(numberCar);
                ch = MainSikulixStartParkomatic.oplataDo(numberCar, numberOfParking);
            }
        }

        if (ch)
        {
            result = "success pay";
        }
        else
        {
            result = "no success pay";
        }
        return result;
    }
    @Override
    public String cancel()
    {
        String result="";
        boolean check = false;
        check = MainSikulixStartParkomatic.сancel();
        if (check)
        {
            result = "success cancel";
        }
        else
        {
            result = "no success cancel";
        }

        return result;
    }



    @Override
    public String renew()
    {
        String result="";
        boolean check = false;
        check = MainSikulixStartParkomatic.renew();
        if (check)
        {
            result = "success renew";
        }
        else
        {
            result = "no success renew";
        }

        return result;
    }

    @Override
    public void setup() {

        System.out.println("Setting up datasource for DEV environment. ");

    }


}
