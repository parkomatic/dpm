package springboot.topjava.ru.BD;

import java.sql.*;

public class BDNumberModem {
    public static String [] parkomativBDnumPhone(int idnum) {

            Connection c = null;
            PreparedStatement pstmt=null;
            String  numberphone = null;
            String  pin = null;

            try {
                c=conectionBD(c);

                pstmt=c.prepareStatement("SELECT * FROM NUMBERMODEM WHERE ID=?;");
                pstmt.setInt(1,idnum);
                ResultSet rs = pstmt.executeQuery();
               //ResultSet rs = stmt.executeQuery( "SELECT * FROM NUMBERMODEM WHERE ID="+idnum+";" );
                while ( rs.next() ) {
                    int id = rs.getInt("id");
                    numberphone = rs.getString("NUMBERPHONE");
                    pin = rs.getString("PIN");
                    System.out.println(String.format("ID=%s NUMBERPHONE=%s PIN=%s ",id,numberphone,pin));
                }

                rs.close();
                pstmt.close();
                c.commit();
                System.out.println("-- Operation SELECT done successfully");
                c.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getClass().getName()+": "+e.getMessage());
                System.exit(0);
            }
            return new String[]{numberphone, pin};
        }


    public static int  countaccesskey(String access) {
        Connection c=null;
        PreparedStatement pstmt=null;
        int count=0;
        try {
            c=conectionBD(c);

            pstmt=c.prepareStatement("SELECT COUNT(*) FROM accesskey where val=?;");
            pstmt.setString(1,access);
            ResultSet rs= pstmt.executeQuery();
           // ResultSet rs = stmt.executeQuery( "SELECT COUNT(*) FROM accesskey where val='"+access+"';" );
            while ( rs.next() ) {
                count = (int) rs.getInt("count");

                System.out.println(String.format(String.valueOf(count)));
            }

            rs.close();
            pstmt.close();
            c.commit();
            System.out.println("-- Operation SELECT done successfully");
            c.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }

        return count;
    }
    public static int countNumber()
    {
        Connection c = null;
        PreparedStatement pstmt=null;
        int count=0;

        try {
            c=conectionBD(c);

            pstmt=c.prepareStatement("SELECT COUNT(*) FROM NUMBERMODEM ");
            ResultSet rs = pstmt.executeQuery();
            //ResultSet rs = stmt.executeQuery( "SELECT * FROM NUMBERMODEM WHERE ID="+idnum+";" );
            while ( rs.next() ) {
                count = rs.getInt("count");
                System.out.println(String.format(String.valueOf(count)));;
            }

            rs.close();
            pstmt.close();
            c.commit();
            System.out.println("-- Operation SELECT done successfully");
            c.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        return count;
    }
    public static Connection conectionBD(Connection c)
    {
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/parkomatic", "postgres", "postgres");
            c.setAutoCommit(false);
            System.out.println("-- Opened database successfully");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        return c;
    }

}
