package springboot.topjava.ru.api_auth;

public class RequestReset {
    private String Data;
    private String sessionKey;
    private String action;

    public RequestReset() {
    }

    public RequestReset(String data, String sessionKey, String action) {
        Data = data;
        this.sessionKey = sessionKey;
        this.action = action;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
