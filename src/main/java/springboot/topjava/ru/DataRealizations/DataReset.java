package springboot.topjava.ru.DataRealizations;

public class DataReset implements Data{
    String pole;

    public DataReset() {
    }
    public DataReset(String pole) {
        this.pole = pole;
    }

    public String getPole() {
        return pole;
    }

    public void setPole(String pole) {
        this.pole = pole;
    }

}
